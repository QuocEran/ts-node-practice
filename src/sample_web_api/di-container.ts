import { Container } from "inversify";
import { DBService } from "./mongo/db.service";
import { SubscriberRepository } from "./mongo/subscribers/subscribers.repository";
import { SubscribersService } from "./core/subscriber.services";

export const container = new Container({ defaultScope: "Singleton" });

container.bind(DBService).toSelf();
container.bind(SubscriberRepository).toSelf();
container.bind(SubscribersService).toSelf();
