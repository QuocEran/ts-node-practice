import {
  controller,
  httpDelete,
  httpGet,
  httpPatch,
  httpPost,
} from "inversify-express-utils";
import { SubscribersService } from "src/sample_web_api/core/subscriber.services";
import { Request, Response } from "express";
@controller("/subscribers")
export class SubscribersController {
  constructor(private readonly _service: SubscribersService) {}

  @httpGet("/")
  async index(req: Request, res: Response) {
    const subscribers = await this._service.all();

    res.status(200).json({
      data: {
        subscribers,
      },
    });
  }

  @httpGet("/:id")
  async findOne(req: Request, res: Response) {
    const subscriber = await this._service.findOne(req.params.id);

    res.status(200).json({
      data: {
        subscriber,
      },
    });
  }

  @httpPost("/")
  async store(req: Request, res: Response) {
    const subscriber = await this._service.create(req.body);

    res.status(201).json({
      data: {
        subscriber,
      },
    });
  }

  @httpPatch("/:id")
  async update(req: Request, res: Response) {
    const updatedSubscriber = await this._service.updateOne(
      req.params.id,
      req.body
    );

    res.json({
      data: {
        subscriber: updatedSubscriber,
      },
    });
  }

  @httpDelete("/:id")
  async destroy(req: Request, res: Response) {
    await this._service.deleteOne(req.params.id);

    res.sendStatus(204);
  }
}
