import "dotenv/config";
import "reflect-metadata";
import "./application/controllers/subscribers.controller";
import { App } from "./application/app";

// console.clear();

export async function bootstrap() {
  new App().setup();
}

bootstrap();
