import mongoose from "mongoose";
import { subscribersModel } from "./subscribers/subscribers.model";
import { injectable } from "inversify";

@injectable()
export class DBService {
  private _db!: typeof mongoose;

  async connect() {
    this._db = await mongoose.connect(process.env.DB_URI);
    console.log("connected to DB");
  }

  get subscriber() {
    return this._db.model("Subscriber", subscribersModel);
  }
}
