/**
 * Mọi đối tượng trong JavaScript sẽ đều tuân theo mô hình như trên. Mỗi đối tượng sẽ có một thuộc tính đặc biệt, gọi là prototype (nguyên mẫu) với tên chuẩn là [[Prototype]]. Bản thân prototype cũng là một đối tượng và nó sẽ có prototype của nó tạo thành một chuỗi prototype.

Chuỗi prototype sẽ kết thúc sau các đối tượng dựng sẵn của JavaScript (function, Object, v.v…) hoặc các đối tượng khác khi mà prototype của những đối tượng này là null.

Khi bạn truy cập một thuộc tính của đối tượng, việc truy xuất sẽ diễn ra thế này: nếu đối tượng không có sẵn thuộc tính đó, prototype của nó sẽ tìm các thuộc tính của mình. Nếu thuộc tính vẫn không tìm thấy, prototype của prototype sẽ tìm kiếm. Cứ như vậy, cho đến khi tìm thấy thuộc tính hoặc kết thúc chuỗi prototype thì dừng lại. Đây là sự ủy quyền (delegation).

Một điểm khác biệt giữa các thức hoạt động dựa trên prototype so với class truyền thống đó là việc tham chiếu các thuộc tính. Ví dụ chúng ta đã có 2 object của Animal, nếu xóa đi một phương thức của object leo, điều gì sẻ xảy ra?

Lúc này, không phải chỉ đối tượng leo mới bị mất phương thức, mà tất cả các đối tượng khác cũng không còn thuộc tính này:

delete Object.getPrototypeOf(leo).play;
leo.play; //undefined
snoop.play; // undefined
Nguyên nhân là bởi vì tất cả những đối tượng này cùng tham chiếu đến một prototype (tức là prototype là một object được dùng chung, không được clone mà chỉ tham chiếu).

Qua những ví dụ này thì cơ chế prototype của JavaScript cũng đã được hé lộ phần nào. Việc hoạt động dựa trên prototype không có gì là xấu, nó cho phép lập trình viên được tự do hơn trong lập trình, ít bị gò bó như những ngôn ngữ khắt khe như Java.

Hơn nữa cơ chế này đơn giản hơn nên hiệu suất cũng tốt hơn rất nhiều (có ý nghĩa rất lớn với những script chạy trên client side như JavaScript).
 * 
 */

//class, constructor, super, extends.

class Animal {
  name: string;
  energy: number;
  constructor(name: string, energy: number) {
    this.name = name;
    this.energy = energy;
  }
  eat(amount: number) {
    console.log(`${this.name} is eating.`);
    this.energy += amount;
  }
  sleep(length: number) {
    console.log(`${this.name} is sleeping.`);
    this.energy += length;
  }
  play(length: number) {
    console.log(`${this.name} is playing.`);
    this.energy -= length;
  }
}

class Dog extends Animal {
  superPowr: string;
  constructor(name: string, energy: number) {
    super(name, energy);
    this.superPowr = "Gau";
  }
  bark() {
    console.log(this.superPowr);
  }
}
const henry = new Dog("Henry", 10);

henry.play(10);

henry.bark();
