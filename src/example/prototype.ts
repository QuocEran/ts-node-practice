/**
 * Thế nhưng, với từ khóa new mọi thứ sẽ đơn giản hơn. Khi bạn gọi một hàm với từ khóa new,
 * Object.create sẽ được gọi và việc đó hoàn toàn tự động mà không cần một dòng code nào.
 * Đối tượng được trả về ở đây được gọi là this. Thế nhưng, với từ khóa new mọi thứ sẽ đơn giản hơn.
 * Khi bạn gọi một hàm với từ khóa new, Object.create sẽ được gọi và việc đó hoàn toàn tự động mà không cần một dòng code nào.
 * Đối tượng được trả về ở đây được gọi là this.
 *
 * Ở đây, chúng ta có thể thấy rõ ràng rằng, Object.create không tạo ra một đối tượng mới với các thuộc tính của đối tượng cũ.
 * Nó tạo ra một đối tượng mới với prototype là đối tượng cũ, và sẽ ủy quyền để truy vấn các thuộc tính này.
 * Bản thân <prototype> cũng như các thuộc tính của nó lại có prototype của riêng mình (object hoặc function),
 * vì vậy đây gọi là chuỗi prototype.
 *
 *
 * Lưu ý rằng, theo chuẩn ECMAScript thì thuộc tính này có tên là [[Prototype]] và
 * phương thức để truy cập prototype là Object.getPrototypeOf.
 *
 * Nhưng tôi đang dùng Firefox nên sẽ gọi theo kiểu Firefox vẫn đang gọi (<prototype>)
 *
 * Để truy cập thuộc tính này, các trình duyệt (riêng IE thì chỉ có IE 11) đều cài đặt
 * thuộc tính __proto__ để làm việc đó. Phương thức này không phải là đối tượng <prototype> mà là
 * getter/setter để truy cập đến đối tượng này.
 */

const prototype = () => {
  interface IAnimal {
    name: string;
    energy: number;
  }

  function Animal(this: any, name: string, energy: number) {
    this.name = name;
    this.energy = energy;
  }

  Animal.prototype.eat = function (amount: number) {
    console.log(`${this.name} is eating.`);
    this.energy += amount;
  };

  Animal.prototype.sleep = function (length: number) {
    console.log(`${this.name} is sleeping.`);
    this.energy += length;
  };

  Animal.prototype.play = function (length: number) {
    console.log(`${this.name} is playing.`);
    this.energy -= length;
  };

  const leo = new (Animal as any)("Leo", 7);

  console.info(leo);

  // Nếu không dùng class và extend thì cú pháp như sau
  function Dog(this: any, name: string, energy: number) {
    Animal.call(this, name, energy);
    this.superPower = "ROAR!";
  }

  Dog.prototype = Object.create(Animal.prototype);
  Dog.prototype.bark = function () {
    console.log("BARK!");
  };
  const henry = new (Dog as any)("henry", 10);
  console.log(typeof henry);
  henry.bark();
  henry.play(10);
};
export default prototype;
