export class Container {
  private readonly _deps: Record<string, any> = {};
  private _key: string = "";

  bind(key: string) {
    this._key = key;
    return this;
  }

  to(dep: any) {
    this._deps[this._key] = new dep();
    this._key = "";
  }

  get<T>(key: string) {
    return this._deps[key] as T;
  }
}
